#include <stdio.h>
#include <stdlib.h>
#include "funciones.h"

   
    //Decraracion del puntero que permitira llamar la funcion calcularPromedioPaginas
    float (*funcion) (Version* versiones);
    main(){

        int opcion = 4;
    //Declaracion de un puntero de tipo Libro
        Libro *libro;
    //banderas que permiten comprobar si se registro un libro y si se ha calculado el promedio de paginas
        int b1 = 0;
        int b2 = 0;
        int i;
        int j=0;
        do{
            printf("********************** MENU ************************ \n");
            printf("| 1. Registrar Libro |\n");
            printf("| 2. Calcular Promedio de paginas de los capitulos |\n");
            printf("| 3. Imprimir Libro |\n");
            printf("| 4. Salir |\n");
            printf("********************************************************\n");
            printf("Seleccione la opcion: ");
            scanf("%d",&opcion);
                switch(opcion){

            //Opcion para registrar libro
                case 1:
            //Separacion de memoria dinamica para el puntero libro
                    libro = (Libro*)malloc(sizeof(Libro));
                    fflush( stdin);

                    printf("Ingrese el Nombre del libro: ");
                    scanf("%*c%[^\n]", libro->nombreLibro);
                    fflush( stdin );

                    printf("Ingrese el Nombre Autor: ");
                    scanf("%*c%[^\n]", libro->nombreAutor);
                    fflush( stdin );

                    printf("Ingrese el Codigo del libro: ");
                    scanf("%d", &libro->codigo);
                    fflush( stdin );
                    libro->versiones =(struct Version*)malloc(10*sizeof(struct Version));
                    for(i=0; i<3; i++){
                        
                        printf("\nIngrese el numero de capitulos para version %d del libro: " ,i+1);
                        
                        scanf("%d",&libro->versiones[i].numCapitulos);
                        fflush( stdin );
                        printf("\n Registro del numero de paginas por capitulo \n\n");
                        libro->versiones[i].capitulos = (struct Capitulo*)malloc(20*sizeof(Capitulo));
                        for(j=0; j<libro->versiones[i].numCapitulos; j++){
                            printf("Ingrese el numero de paginas del capitulo %d : ",j+1);
                            scanf("%d",&(libro->versiones[i].capitulos[j].numeroPaginas));
                            
                        }
                       
                    }
                    //printf("numero paginas dela version 2 capitulo 2 %d ",*(libro->versiones[i].capitulos[j].numeroPaginas);
                    printf("\n");
                    b1 = 1;
                    b2 = 0;
                    break;
            //Opcion para calcular el promedio
                case 2:
                    if(b1 == 1){
                        funcion = calcularPromedioPaginas;
                        libro->promedioPaginas = funcion(libro->versiones);
                        printf("El promedio de paginas por capitulo de todas las versiones es: %.2f\n\n",libro->promedioPaginas);
                        b2 = 1;
                    }
                    else{
                        printf("Debe registrar primero el libro: Opcion 1.\n\n");
                    }
                    break;
            //Opcion que muestra los datos del libro
                case 3:
                    if(b2 == 1){
                        printf("Nombre del libro: %s \n",libro->nombreLibro);
                        printf("Nombre del Autor: %s \n",libro->nombreAutor);
                        printf("Codigo del libro: %d \n",libro->codigo);
                        int i;
                        int j;
                        float nPaginas=0;
                        for(i=0; i<3; i++){ 

                            printf("Informacion de la version %d",i+1);
                            printf("\n\tNumero de Capitulos: %d",libro->versiones[i].numCapitulos);
                            for(j=0; j<libro->versiones[i].numCapitulos; j++){
                                printf("\n\t\tNumero de paginas del capitulo %d :",j+1);
                                printf("--> %d ",libro->versiones[i].capitulos[j].numeroPaginas);
                                nPaginas+=libro->versiones[i].capitulos[j].numeroPaginas;
                            }
                            printf("\n\tPromedio de paginas por Version: %.2f\n\n",(nPaginas/libro->versiones[i].numCapitulos));
                            nPaginas=0;
                        }
                    }
                    else{
                        printf("\nDebe calcular primero el promedio de paginas de los capitulos: Opcion 2.\n\n");
                    }
                    break;
            //Opcion para salir de la aplicacion
                case 4:
                    free(libro);
                    break;
                default: break;
            }
        }while(opcion != 4);
            return(0);
    }
