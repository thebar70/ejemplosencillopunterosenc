#include <stdio.h>
#include <stdlib.h>
#include "funciones.h"
//Funcion para calcular el promedio de paginas de los capitulos
//Ingresa el vector que contiene los capitulos y n: indica el tamaño del vector
float calcularPromedioPaginas(Version* versiones){
    int i=0;
    int j=0;
    float nPaginas = 0;
    int temporal = 0;
    float nCapitulos = 0;
    float promedio = 0;
    for(i=0;i<3;i++)
    {
        nCapitulos = nCapitulos + versiones[i].numCapitulos;
        temporal = versiones[i].numCapitulos;
        for(j=0;j<temporal;j++)
        {
            nPaginas = nPaginas + (versiones[i].capitulos[j]).numeroPaginas;
        }
        
    }

    promedio=nPaginas/nCapitulos;
    return promedio;
}
