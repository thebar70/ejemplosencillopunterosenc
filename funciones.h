/*Declaracion de las funciones utilizadas por gestion de libros*/
//structs que contienen los atributos de Libro y Capitulo
    struct Capitulo{
        int numeroPaginas;
    }typedef Capitulo;

     struct Version{
        int numCapitulos;
        struct Capitulo* capitulos;
        
    }typedef Version;

    struct Libro{
        char nombreLibro[30];
        char nombreAutor[30];
        struct Version* versiones;
        int codigo;
        
        float promedioPaginas;
    }typedef Libro;



float calcularPromedioPaginas(Version* versiones);
